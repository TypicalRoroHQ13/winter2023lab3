//Made by Safin Haque 2132492

import java.util.Scanner;

public class NationalPark{
	public static void main(String[]args){
		Scanner reader=new Scanner(System.in);
		
		Cow cows=new Cow();
		
		Cow[] herdOfCows=new Cow[4];
		
		for(int i=0;i<herdOfCows.length;i++)
		{
			herdOfCows[i]=new Cow();
		}
		
		for(int j=0;j<herdOfCows.length;j++)
		{
			System.out.println("How old is cow " + (j+1) + " in Months?");
			herdOfCows[j].ageInMonths=reader.nextInt();
			
			System.out.println("How much does cow " + (j+1) + " weigh in kg?");
			herdOfCows[j].sizeInKg=reader.nextDouble();
			
			System.out.println("Is cow " + (j+1) + " a Heifer? true or false");
			herdOfCows[j].isHeifer=reader.nextBoolean();
		}
		System.out.println();
		
		System.out.println(herdOfCows[3].ageInMonths);
		System.out.println(herdOfCows[3].sizeInKg);
		System.out.println(herdOfCows[3].isHeifer);
		
		System.out.println();
		
		herdOfCows[0].cowBio();
		herdOfCows[0].BreedorNay();
	}
	
}